#!/bin/sh
rm "$2.out" 2> /dev/null
mv "$1.txt" "$1.m"
if [ -f STOP_OPTIMIZATION ]; then
    exit
fi
oldpwd=$PWD
/usr/bin/matlab -nodesktop -nosplash -r "cd '/home/rasuth/opt/openEMS/share/openEMS/matlab'; optimizer_asco_sim('/home/rasuth/openEMS/matlab/examples/optimizer/opttmp','$1','$2.out','/home/rasuth/openEMS/matlab/examples/optimizer/optimizer_simfun.m'); exit"
cd "$oldpwd"
