# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a fork to the public domain OpenEMS software.  OpenEMS is a 3D FDTD E/M simulator using Octave (or MATLAB) as a scripting
tool for pre- and post-processing.  As such it provides powerful file handling (HDF5) and Imaging (2D, 3D, and animation) as well as
raw data manipulation.

### How do I get set up? ###

As a git repository, this can be downloaded to an appropriate Linux computer and assuming the environment is correctly set up, can
be immediately built and used for E/M simulation.  There will be documentation accumulated over time to streamline this process.  It is also possible
that we may go to a 'Docker' architecture that downloads everything needed, with the appropriate versions, to instantiate a working 
'container' on a Linux computer.

### Contribution guidelines ###

Please do not commit any changes to the 'main' trunk without review.  

### Who do I talk to? ###

Talk to Bob Sutherland
